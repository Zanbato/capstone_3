const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken'); 
const config = require('./config.js');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');  
const moment = require('moment');

mongoose.connect(config.databaseURL, { useNewUrlParser: true }); 

require('./passport.js'); 

const app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(passport.initialize());	

const index = require('./routes/index.js');

app.use('/', index);

const auth = require('./routes/auth.js');

app.use('/auth', auth);

const cinemas = require('./routes/cinemas');

app.use('/cinemas', cinemas);

const movies = require('./routes/movies');

app.use('/movies', movies);

const user = require('./models/User.js');

app.use('/users', passport.authenticate('jwt', {session: false}), user);

const transaction = require('./routes/transactions.js'); 

app.use('/transactions', passport.authenticate('jwt', {session: false}) ,transaction);

app.use(function(req, res, next) {
  var err = new message('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.json({ 'error': err.message });
});

app.listen(config.port, () => {
  console.log('Server is running at:', config.port);
});

//export the app
module.exports = app;