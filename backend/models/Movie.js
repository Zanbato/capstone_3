const mongoose = require('mongoose');

const Schema = mongoose.Schema;

//Create a new Schema for the Availability
const MovieSchema = new Schema({
    cinema_id: String,
    cinema_name: String,
    name: String,
    description: String,
    rating: String,
    running_time: String,
    date: String,
    timeSlot: [
    {
        showingTime: String,
        seatsAvailable: Number,
        isActive: {type: Boolean, default: true}
    }
    ],
    price: Number,
    poster: String,
    trailer: String,
    is_public: {type: Boolean, default: false}
});

module.exports = mongoose.model('movie', MovieSchema);