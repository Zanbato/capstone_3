const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	firstName: String,
	lastName: String,
	email: {type: String, unique: true},
	password: String,
	isAdmin: {type: Boolean, default: false} 
});

//JWT generation method to the Schema
UserSchema.methods.generateJWT = function() {
	
	const today = new Date();

	const expirationDate = new Date(today);

	expirationDate.setMinutes(today.getMinutes() + 30);

	return jwt.sign({
		_id: this._id,
		email: this.email,
		isAdmin: this.isAdmin,
		exp: parseInt(expirationDate.getTime() / 1000, 10)
	}, 'lalilulelo');
}

//JSON generation method to the schema
UserSchema.methods.toAuthJSON = function() {
	return {
		_id: this._id,
		email: this.email,
		firstName: this.firstName,
		lastName: this.lastName,
		isAdmin: this.isAdmin,
		token: this.generateJWT()
	};
};

module.exports = mongoose.model('user', UserSchema);