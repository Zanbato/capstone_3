const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TransactionSchema = new Schema({ 
    ownerEmail: String, 
    movieSlot: Schema.Types.ObjectId, 
    movieId: Schema.Types.ObjectId, 
    firstName: String, 
    lastName: String, 
    total: Number,
    quantity: Number,
    status: {type: String, default: "booked"},
    date: String
});

module.exports = mongoose.model('transaction', TransactionSchema);