const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CinemaSchema = new Schema({
    name: String,
    seatsAvailable:Number
});

module.exports = mongoose.model('cinema', CinemaSchema);