// Export configuration variables
module.exports = {

	// Establish the port where the application will be run on
	'port': process.env.PORT || 3000,
	
	// Specify the app secret key
	'secretKey': process.env.SECRET_KEY || 'lalilulelo',

	// Specify the database URL
	// 'databaseURL': process.env.DATABASE_URL || 'mongodb://localhost:27017/capstone3_movie_db',
	'databaseURL': process.env.DATABASE_URL || 'mongodb+srv://admin:Deejoriza95.@cluster0-jsn21.mongodb.net/capstone3_movie_db?retryWrites=true',
	
	// Establish salt rounds
	'saltRounds': 10,

	// Set the Stripe secret key
	'stripeSecretKey': process.env.STRIPE_SECRET_KEY || '?',

};