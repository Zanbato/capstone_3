const express = require('express');

const router = express.Router();

// const jwt = require('jsonwebtoken');

const passport = require('passport');

// const appPassport = require('../passport.js');

// const config = require('../config.js');

// login
router.post('/login', (req, res, next) => {

    passport.authenticate('local', {session: false}, (err, user, info) => {

      if (err || !user) {
          return res.status(400).send({
            'error': info,
           });
      }

      req.login(user, {session: false}, (err) => {
        if (err) {
          res.send(err);
        }

      const token = user.toAuthJSON();

      return res.status(200).json({
          'data': token
      });
    });

  })(req, res, next);

});

//logout user at GET request
router.get('/logout', function(req, res){
  req.logout();
  return res.send({
    'message' : 'logout'
  });
});

//export the router
module.exports = router;