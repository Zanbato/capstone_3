const express = require('express');
const router = express.Router();
const TransactionModel = require('../models/Transaction.js');
const config = require('../config.js');
const stripe = require("stripe")(config.stripeSecretKey);
const moment = require("moment");
const MovieModel = require('../models/Movie.js');
const UserModel = require('../models/User.js');

router.post('/', (req, res) => {
	MovieModel.findOne({ "timeSlot._id": req.body.timeSlot_id }).then( (movie) => {

		let cinema = null;

		let timeSlots = movie.timeSlot;

		timeSlots.forEach(function(timeslot){
			if(timeslot._id == req.body.timeSlot_id)
				cinema = timeslot;
		})

		//instantiate a new transaction, saving it as a variable named newTransaction
		let newTransaction = new TransactionModel({
			'ownerEmail': req.body.email,
			'movieSlot': req.body.timeSlot_id,
			'movieId': movie._id,
			'firstName': req.body.firstName,
			'lastName': req.body.lastName,
			'total': req.body.quantity * movie.price,
			'quantity': req.body.quantity,
			'date' : moment().format('MMMM Do YYYY, h:mm:ss a') 
		});

		//If no availability found
		if(!movie){
			return res.status(500).send({ 
				'error': 'no movie found',
			});
		}

		if(cinema.seatsAvailable == 0){
			return res.status(500).send({
				'error': 'no more seats are available',
			});
		}

		//if there are seats available and the quantity to be booked is not more the the available seats
		if(cinema.seatsAvailable > 0 && req.body.quantity <= cinema.seatsAvailable){

			cinema.seatsAvailable = cinema.seatsAvailable - req.body.quantity;

			if(cinema.seatsAvailable == 0) {
				cinema.isActive = false;
			}

			movie.save();	

			newTransaction.save( (err, transaction) => {
				
				if(err){
					return res.status(500).send({
						'error': err 
					});
				}

				//If database operation is successful, return a json response containing the transaction object with a status of 200
				return res.status(200).send({
					'message': transaction
				});

			});

		} else {
			//return a json response
			return res.send({
						'message': 'insufficient seats for intended booking'
					});
		}
	})
	.catch( (err) => {
		return res.send({ 
			'err': err,
		});
	});
});


router.get('/all', (req, res) => {
	TransactionModel.find({}).then( (transactions) => {

		//If no transactions are found, 
		if(!transactions){
			return res.status(200).send({ 
				'message': 'no transactions'
			});
		}

		//otherwise, return the transactions object within the transactions property of the data property with a status of 200
		return res.status(200).send({ 
			transactions
		});
	});
});

router.get('/movie/:id', (req, res) => {
	TransactionModel.find({"movieId": req.params.id}).then( (transactions) => {

		//If no transactions are found, 
		if(!transactions){
			return res.status(200).send({ 
				'message': 'no transactions'
			});
		}

		//otherwise, return the transactions object within the transactions property of the data property with a status of 200
		return res.status(200).send({ 
			transactions
		});
	});
});

router.get('/:id', (req, res) => {
	UserModel.findOne({'_id': req.params.id}).then( (user) => {
		//if no user found
		if(!user){
			return res.status(200).send({
				'message': 'no user found'
			});
		}

		let email = user.email;
		
		TransactionModel.find({'ownerEmail': email}).then( (transactions) => {
			// If no transactions are found, 
			if(!transactions){
				return res.status(200).send({ 
					'message': 'no transactions'
				});
			}

			return res.status(200).send({ 
 				transactions
			});
		});
	});
});

router.post('/:id', (req,res) => {
	let oldQuantity;
	let oldMovieSlot;
	TransactionModel.findOne({'_id' : req.params.id})
		.then(async (transaction) => {
			if(transaction) {
				//save it as a variable named oldTransaction
				let oldTransaction = transaction;

				let newTransaction = new TransactionModel({
					'ownerEmail': oldTransaction.ownerEmail,
					'movieSlot': oldTransaction.movieSlot,
					'movieId': oldTransaction.movieId,
					'firstName': oldTransaction.firstName,
					'lastName': oldTransaction.lastName,
					'quantity': -oldTransaction.quantity,
					'total' : -oldTransaction.total,
					'status' : req.body.status,
					'date' : moment().format('MMMM Do YYYY, h:mm:ss a')
				});

				oldQuantity = oldTransaction.quantity;
				oldMovieSlot = String(oldTransaction.movieSlot);

				await MovieModel.findOne({'timeSlot._id':oldTransaction.movieSlot}).then((movie)=>{
					let time = movie.timeSlot;
					let cinema = null;
					time.forEach(function(timeslot){
						if(timeslot._id == oldMovieSlot){
							cinema = timeslot
						}
					})

					cinema.seatsAvailable = cinema.seatsAvailable + oldQuantity;

					movie.save();
					
				})
				await TransactionModel.deleteOne({'_id' : req.params.id});

				newTransaction.save( (err, transaction) => {
				
					//if an error is thrown
					if(err){
						return res.status(500).send({
							'error': err 
						});
					}

					return res.status(200).send({
						'data' : {
							'message' : 'successful cancellation',
							'transaction' : transaction
						}
					});
				});
			}
			//if there's no transaction found
			else {
				return res.send({
					'message' : 'no transaction was found'
					});
			}
		});
});

//export the router object
module.exports = router;