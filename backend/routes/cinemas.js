const express = require('express');

const router = express.Router();

const CinemaModel = require('../models/Cinema.js');

router.get('/', (req, res) => {

	CinemaModel.find({}, (err, cinema) => {
		if(!cinema){
			return res.status(200).send({
				'info': 'nothing available at the moment',
			});
		}

		return res.status(200).send({
			'cinema': cinema,
		});
	});

});

router.post('/', (req, res) => {

	let newCinema = CinemaModel({
		'name': req.body.name,
		'seatsAvailable': req.body.seatsAvailable,
	});

	newCinema.save( (err, cinema) => {

		//If an error is thrown
		if(err){
			//return a response with a status of 500 and a json object with an error property containing the thrown error
			return res.status(500).send({
				'error': err
			});
		}
		//if database operation is successful, return a response with a status of 200 and a json object with a data property
		return res.status(200).send({
			'data': {
				'message': 'successful',
				'availability': cinema
			}
		});
	});
});

router.get('/:id', (req, res) => {

	CinemaModel.findOne({ '_id': req.params.id }).then( (cinema) => {

		if(!cinema){
			return res.status(200).send({
				'message': 'no cinema found'
			});
		}

		return res.status(200).send({
				'availability': cinema
		});

	});

});

router.put('/:id', (req, res) => {
	CinemaModel.update( { '_id' : req.params.id}, req.body )
		.then( (result) => {
			if(result) {
				return res.send({
					'data' : {
						'availability' : result,
						'message' : 'successful update'
					}
				});
			}
			return res.json({
				'message' : 'no such availability was found'
			});
		});
});

router.delete('/:id', (req, res) => {
	CinemaModel.deleteOne( { '_id' : req.params.id})
		.then( (result) => {
			//if availability found,
			if(result) {
				//return a json response
				return res.send({
					//with a data property
					'data' : { //that has the following properties
						//the updated availability object
						'availability' : result,
						//a notification message for the successful update
						'message' : 'delete success'
					}
				});
			}
			//otherwise, return a json response 
			return res.json({
				//with a message property named message stating that no such availability was found
				'message' : 'no such availability was found'
			});
		});
});


//export the router object
module.exports = router;