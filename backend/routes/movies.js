const express = require('express');

const router = express.Router();

const MovieModel = require('../models/Movie.js');
const CinemaModel = require('../models/Cinema.js');


router.get('/', (req, res) => {

	MovieModel.find({}, (err, movie) => {

		if(!movie){
			return res.status(200).send({
				'info': 'nothing available at the moment',
			});
		}

		return res.status(200).send({
				'movies': movie,
		});
	});

});

router.post('/', async (req, res) => {
	let cinema = await CinemaModel.findOne({ '_id': req.body.cinema_id }).then( (cinema) => {

		if(!cinema){
			return res.status(200).send({
				'message': 'no cinema found'
			});
		}
		return cinema
	});
	let timeSlots= req.body.timeSlots;
	let timeSlotArray=[];

	timeSlots.forEach(function(timeSlot){
		let timeSlotObject={};
		timeSlotObject.showingTime = timeSlot;
		timeSlotObject.seatsAvailable = cinema.seatsAvailable;
		timeSlotArray.push(timeSlotObject);
	})

	let newMovie = MovieModel({
		'cinema_id': req.body.cinema_id,
		'cinema_name': cinema.name,
		'name': req.body.name,
		'description': req.body.description,
		'rating': req.body.rating,
		'running_time': req.body.running_time,
		'date': req.body.date,
		'timeSlot' : timeSlotArray,
		'price' : req.body.price,
		'poster' : req.body.poster,
		'trailer' : req.body.trailer,
		'is_public' : req.body.is_public,
	});

	newMovie.save( (err, movie) => {
		if(err){
			return res.status(500).send({
				'error': err
			});
		}
		return res.status(200).send({
			'data': {
				'message': 'successful create',
				'availability': movie
			}
		});
	});
});

router.get('/:id', (req, res) => {

	MovieModel.findOne({ '_id': req.params.id }).then( (movie) => {

		if(!movie){
			return res.status(200).send({
				'message': 'not found'
			});
		}

		return res.status(200).send({
			movie
		});

	});

});

router.put('/:id', async (req, res) => {
	let cinema = await CinemaModel.findOne({ '_id': req.body.cinema_id }).then( (cinema) => {

		if(!cinema){
			return res.status(200).send({
				'message': 'no cinema found'
			});
		}
		return cinema
	});

	let timeSlots= req.body.timeSlots;
	let timeSlotArray=[];
	timeSlots.forEach(function(timeSlot){
		let timeSlotObject={};
		timeSlotObject.showingTime = timeSlot;
		timeSlotObject.seatsAvailable = cinema.seatsAvailable;
		timeSlotObject.reservations = [];
		timeSlotArray.push(timeSlotObject);
	})

	req.body.timeSlot = timeSlotArray;
	req.body.cinema_name= cinema.name;

	MovieModel.updateOne( { '_id' : req.params.id}, req.body )
		.then( (movie) => {
			if(movie) {
				return res.send({
					'data' : {
						'availability' : movie,
						'message' : 'updated'
					}
				});
			}
			return res.json({
				'message' : 'some kind of error'
			});
		});
});

router.put('/public/:id', async (req, res) => {
	MovieModel.updateOne( { '_id' : req.params.id}, req.body )
		.then( (movie) => {
			if(movie) {
				return res.send({
					'data' : {
						'availability' : movie,
						'message' : 'updated'
					}
				});
			}
			return res.json({
				'message' : 'some kind of error'
			});
		});
});

router.delete('/:id', (req, res) => {
	MovieModel.deleteOne( { '_id' : req.params.id})
		.then( (result) => {
			//if availability found,
			if(result) {
				//return a json response
				return res.send({
					//with a data property
					'data' : { //that has the following properties
						//the updated availability object
						'availability' : result,
						//a notification message for the successful update
						'message' : 'delete success'
					}
				});
			}
			//otherwise, return a json response 
			return res.json({
				//with a message property named message stating that no such availability was found
				'message' : 'no such availability was found'
			});
		});
});

//export the router object
module.exports = router;