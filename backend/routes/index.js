const express = require('express');
const passport = require('passport');
const config = require('../config.js');
const UserModel = require('../models/User.js');
const bcrypt = require('bcrypt-nodejs');
const router = express.Router();

router.get('/', passport.authenticate('jwt', {session: false}), function(req, res){
	res.send('hello '+req.user.firstName)
})


router.post('/register', (req,res) => {
	let firstName = req.body.firstName;
	let lastName = req.body.lastName;
	let email = req.body.email;
	let password = req.body.password;

	//if either email or password is not submitted
	if(!email || !password) {
		return res.status(500).send({
			'error' : 'Missing email/password'
		});
	}

	//search for any duplicate user records in the database
	UserModel.find({'email' : email})
		.then((users, err) => {

			//if an error is thrown
			if(err) {
				return res.status(500).send({
					'error' : err
				});
			}

			//if the length property of the users object is greater than 0
			if(users.length > 0) {
				return res.status(500).send({
					'error' : 'user already exists'
				});
			}

			bcrypt.genSalt(config.saltRounds, function(err, salt) {
				bcrypt.hash(password, salt, null, function(err, hash) {
					let newUser = UserModel({
						'firstName' : firstName,
						'lastName' : lastName,
						'email' : email,
						'password' : hash
					});

					newUser.save((err) => {
						if(!err) {
							return res.send({
								'message' : 'successful registration'
							});
						}
					});
				});
			});
		});
});

//export the router object
module.exports = router;