const passport = require('passport');

const passportJWT = require('passport-jwt');

const ExtractJWT = passportJWT.ExtractJwt;

const LocalStrategy = require('passport-local').Strategy;

const JWTStrategy = passportJWT.Strategy;

const UserModel = require('./models/User.js');

const bcrypt = require('bcrypt-nodejs');

const moment = require('moment');

passport.serializeUser((user, done) => {
	if(!user._id){
		user._id = user.sub;
	}
	//passing in the _id property of the user object as the second argument of the done function saves the property in the session
  	done(null, user._id);
});

passport.deserializeUser((id, done) => {
  done(null, user._id);
});


passport.use(new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {

	UserModel.findOne({ 'email': email }).then( (user) =>{
		//if no user found
		if(!user){
			return done(null, false, { message: 'Invalid credentials.\n' });
		}

		//if passed in email is the same as the email property of user
		if(email == user.email){
			let match = bcrypt.compareSync(password, user.password);

			// Return invalid credentials if log in is invalied
			if (!match) {
	        	return done(null, false, { message: 'Invalid credentials.\n' });
	      	}

	      	// Return successful login if a match is found
	      	return done(null, user);

		}

		// Invalidate users
		return done(null, false, { message: 'Invalid credentials.\n' });

	}).catch( function(err) {
		console.log(err)
	});

}));

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : 'lalilulelo'
    },
    function (jwtPayload, cb) { 
        return UserModel.findOne( {'_id' : jwtPayload._id} )
            .then(user => {
                return cb(null, user);
            })
            .catch(err => {
                return cb(err);
            });
    }
));

// Retrieve account information based on the current session
function getDefaultAccountInfo(accounts){
  let defaultAccount = accounts.find ((item) => item.is_default);
  let accountId = defaultAccount.account_id;
  let baseUri =  `${defaultAccount.base_uri}${'/restapi'}`;
  return baseUri;
}

// Export the necessary modules from this library
module.exports = {
	'getDefaultAccountInfo': getDefaultAccountInfo
};