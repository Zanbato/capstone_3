@extends('layouts.app')

@section('title')
My Transactions
@endsection

@section('content')

<div class="card m-4">
            <div class="card-header bg-dark text-white">
                <div>Transaction List</div>
            </div>
            <div class="card-body">
                @if(empty($transactions))
                    <div>No transactions at the moment</div>
                @else
                    <table class="table table-striped">
                            <tr>
                                <th>Transaction ID</th>
                                <th>Movie</th>
                                <th>Date</th>
                                <th>Seats</th>
                                <th>Total</th>
                                <th colspan="2">Status</th>
                            </tr>
                        @foreach ($transactions as $transaction)
                            <tr>
                                <td>
                                    <p class="id">ID#{{$transaction->_id}}</p>
                                </td>
                                <td>
                                    @foreach($movies as $movie)
                                        @foreach($movie->timeSlot as $timeSlot)
                                            @if($timeSlot->_id == $transaction->movieSlot)
                                                <p class="movieName">{{$movie->name}}</p>
                                                <p class="cinemaName">{{$movie->cinema_name}}</p>
                                                <p class="showDate">{{date("g:i a", strtotime($timeSlot->showingTime))}}</p>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </td>
                                <td class="transactionDate">
                                    <p>{{$transaction->date}}</p>
                                </td>
                                <td>
                                    <p class="seats">{{$transaction->quantity}}</p>
                                </td>
                                <td>
                                    <p class="total">&#8369;{{$transaction->total}}</p>
                                </td>
                                <td>
                                    @if($transaction->status == "booked")
                                        <p class="greenMark">Booked</p>
                                    @else
                                        <p class="redMark">Canceled</p>
                                    @endif
                                </td>
                                <td>
                                    @if($transaction->status == "booked")
                                    <button 
                                        class="btn btn-primary showTicket" 
                                        data-id ="{{$transaction->_id}}"
                                        data-toggle="modal" 
                                        data-target="#modalTicket">
                                        Show Ticket
                                    </button>
                                    <button 
                                        class="btn btn-danger cancelBook" 
                                        data-id ="{{$transaction->_id}}"
                                        data-toggle="modal" 
                                        data-target="#modalDelete">
                                        Cancel
                                    </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
        </div>

{{-- Modal delete --}}
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
      <div class="modal-dialog .modal-dialog-centered " role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header ">
            <h5 class="modal-title">Cancel Transaction?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <!-- Modal Body -->
          <div class="modal-body" id="deleteModalBody">Are you sure you want to cancel this transaction?</div>

          <!-- Modal Footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="confirmDelete" data-dismiss="modal">Confirm</button>
          </div>

        </div>
      </div>
    </div>

{{-- Modal show ticket --}}
    <div class="modal fade" id="modalTicket" tabindex="-1" role="dialog">
      <div class="modal-dialog .modal-dialog-centered " role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header ">
            <h5 class="modal-title">Ticket</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <!-- Modal Body -->
          <div class="modal-body" id="ticketModalBody">
            <div class="container">
                
                <h2 id="modalMovie"></h2>
                <h5><span id="modalCinema"></span> - <span id="modalShowDate"></span></h5>
                <hr>
                <div class="row">
                    <div class="col">
                        <p>
                            <strong>Seats:</strong> 
                            <br>
                            <span id="modalSeats"></span>
                        </p>
                        <p>
                            <strong>Total:</strong> 
                            <br>
                            <span id="modalTotal"></span>
                        </p>
                    </div>
                    <div class="col">
                        <p>
                            <strong>Email:</strong>
                            <br>
                            {{reset($transactions)->ownerEmail}}
                        </p>
                        <p>
                            <strong>Transaction ID:</strong>
                            <br>
                            <span id="modalId"></span>
                        </p>
                    </div>
                </div>
              <img src="/assets/qr_img.png" class="mx-auto d-block my-5" alt="qr-code">
                <div id="additionalDetails">
                      
                  <ul>
                      <li>
                          Tickets must be claimed 45 minutes before the screening time otherwise the reservation will be forfeited.
                      </li>
                        <li>
                          To avoid forfeiture, please cancel your reservation an hour before the screening time.
                        </li>
                  </ul>
                  <p>
                    <strong>ONLINE</strong>. Log on to Blockbuster with your email and password. On the My Transactions page, you can view your current and past transactions. Each active reservation has a CANCEL button to cancel any particular reservation. Please cancel ans reservation/s you do not intend to claim.
                </p>
                </div>
            </div>
          </div>

          <!-- Modal Footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>

        </div>
      </div>
    </div>

    <script type="text/javascript">
        let cancelBook = document.querySelectorAll('.cancelBook');
        let showTicket = document.querySelectorAll('.showTicket');

        cancelBook.forEach(function(btn){
            btn.addEventListener('click', function(){
                let id = this.getAttribute('data-id');
                confirmDelete.addEventListener('click', function(){
                    const headers = {
                        'Content-Type' : 'application/json',
                        'Authorization' : "Bearer {{Session::get('user')->token}}",
                    }
                    let json = JSON.stringify({'status' : "cancel"})
                    fetch('https://fathomless-woodland-43704.herokuapp.com/transactions/'+id, {
                        method:'post',
                        headers: headers,
                        body: json
                    })
                    .then(function(res){
                        return res.json();
                    })
                    .then(function(data){
                        location.reload()
                    })
                })
            })
        })

        showTicket.forEach(function(btn){
            btn.addEventListener('click', function(){
                let parent = this.parentElement.parentElement;
                let movie = parent.querySelector('.movieName').innerHTML;
                let cinema = parent.querySelector('.cinemaName').innerHTML;
                let showDate = parent.querySelector('.showDate').innerHTML;
                let total = parent.querySelector('.total').innerHTML;
                let seats = parent.querySelector('.seats').innerHTML;
                let id = parent.querySelector('.id').innerHTML;
                modalMovie.innerHTML = movie;
                modalCinema.innerHTML = cinema;
                modalShowDate.innerHTML = showDate;
                modalTotal.innerHTML = total;
                modalSeats.innerHTML = seats;
                modalId.innerHTML = id;
            })
        })
    </script>

        
            

            
        
@endsection