@extends('layouts.app')

@section('title')
Movie Transactions
@endsection

@section('content')
<h2>{{$movie->name}}</h2>
  <div class="card m-4">
            <div class="card-header bg-dark text-white">
                <div>Transaction List</div>
            </div>
            <div class="card-body">
                @if(empty($transactions))
                    <div>No transactions at the moment</div>
                @else
                <table class="table table-striped">
                        <tr>
                            <th>Date</th>
                            <th>Transaction ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Slot</th>
                            <th>Seats</th>
                            <th>Total</th>
                            <th>Status</th>
                        </tr>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td class="transactionDate">
                                <p>{{$transaction->date}}</p>
                            </td>
                            <td>
                                <p>ID#{{$transaction->_id}}</p>
                            </td>
                            <td>
                                <p>{{$transaction->firstName}} {{$transaction->lastName}}</p>
                            </td>
                            <td>
                                <p>{{$transaction->ownerEmail}}</p>
                            </td>
                            <td>
                                @foreach($movie->timeSlot as $timeSlot)
                                    @if($timeSlot->_id == $transaction->movieSlot)
                                        <p>{{$movie->cinema_name}}</p>
                                        <p>{{date("g:i a", strtotime($timeSlot->showingTime))}}</p>
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                <p>{{$transaction->quantity}}</p>
                            </td>
                            <td>
                                <p>&#8369;{{$transaction->total}}</p>
                            </td>
                            <td>
                                @if($transaction->status == "booked")
                                    <p class="greenMark">Booked</p>
                                @else
                                    <p class="redMark">Canceled</p>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
                @endif
            </div>
        </div>        
@endsection