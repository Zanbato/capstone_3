@extends('layouts.app')

@section('title', 'Now Showing')

@section('content')
<div class="row">

    @foreach($movies as $movie)
    <div class="col-lg-3 col-md-6 pt-3">
        <div class="card movieShow" data-id="{{$movie->_id}}" data-name="{{$movie->name}}">
            <div class="movieDetail" data-toggle="modal" data-target="#modalDetail">
                <img src="{{$movie->poster}}">
                <div class="alignTextBook">
                    <h5>{{$movie->name}}</h5>
                    <h6>Date of Showing: {{date("F j, Y", strtotime($movie->date))}}</h6>
                </div>
            </div>
            @if(Session::has('user') && Session::get('user')->isAdmin)
            <div class="card-footer">
                <div class="row">
                    @foreach($movie->timeSlot as $timeslot)
                    <div class="col-6">
                        {{date("g:i a", strtotime($timeslot->showingTime))}}
                    </div>
                    @endforeach
                </div>
                <hr>
                    <h6>Cinema: {{$movie->cinema_name}}</h6>
                    <h6>Price: &#8369;{{$movie->price}}</h6>
            </div>
                <button class="btn btn-warning publicMovieBtn w-100 mb-2" data-toggle="modal" data-target="#modalPublic">Make Public</button>
                <button class="btn btn-primary editMovieBtn w-100 mb-2" data-toggle="modal" data-target="#modalEdit">Edit</button>
                <button class="btn btn-danger deleteMovieBtn w-100" data-toggle="modal" data-target="#modalDelete">Delete</button>
            @endif
        </div>
    </div>
    @endforeach
        
</div>

{{-- Modal Book delete --}}
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
      <div class="modal-dialog .modal-dialog-centered " role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header ">
            <h5 class="modal-title">Remove Movie?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <!-- Modal Body -->
          <div class="modal-body" id="deleteModalBody">Are you sure you want to remove "<strong><span id="movieTitleDelete"></span></strong>" from Blockbuster?</div>

          <!-- Modal Footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="confirmDelete" data-dismiss="modal">Confirm Delete</button>
          </div>

        </div>
      </div>
    </div>

{{-- Modal Book Go Public --}}
    <div class="modal fade" id="modalPublic" tabindex="-1" role="dialog">
      <div class="modal-dialog .modal-dialog-centered " role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header ">
            <h5 class="modal-title">Go Public?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <!-- Modal Body -->
          <div class="modal-body" id="deleteModalBody">Are you sure you want to make "<span id="movieTitlePublic"></span>" to public? <p class="redMark"><strong>(You can't further edit this movie.)</strong></p></div>

          <!-- Modal Footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success" id="confirmPublic" data-dismiss="modal">Confirm</button>
          </div>

        </div>
      </div>
    </div>

{{-- Modal Book Edit --}}
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header ">
            <h5 class="modal-title">Edit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <!-- Modal Body -->
          <div class="modal-body" id="editModalBody"></div>

        </div>
      </div>
    </div>

{{-- Modal Book details --}}
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header">
            <h3 class="modal-title" id="movieTitle"></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <!-- Modal Body -->
          <div class="modal-body" id="detailModalBody"></div>

        </div>
      </div>
    </div>
    

    <script type="text/javascript">
        let deleteMovieBtn = document.querySelectorAll(".deleteMovieBtn");
        let publicMovieBtn = document.querySelectorAll(".publicMovieBtn");
        let editMovieBtn = document.querySelectorAll(".editMovieBtn");
        let movieDetail = document.querySelectorAll(".movieDetail");
        let modalDetail = document.querySelector("#modalDetail");

        // For delete movie
        deleteMovieBtn.forEach(function(btn){
            btn.addEventListener("click", function(){
                let id = this.parentElement.getAttribute("data-id");
                let name = this.parentElement.getAttribute("data-name");
                movieTitleDelete.innerHTML = name;
                confirmDelete.setAttribute("data-id", id);
                confirmDelete.addEventListener('click', function(){
                    fetch("https://fathomless-woodland-43704.herokuapp.com/movies/"+id,{
                        method: 'DELETE'
                    })
                    .then (function(res){
                        return res.text();
                    })
                    .then (function(data){
                        location.reload();
                    });
                })
            })
        })

        // for public movie
        publicMovieBtn.forEach(function(btn){
            btn.addEventListener("click", function(){
                let id = this.parentElement.getAttribute("data-id");
                let name = this.parentElement.getAttribute("data-name");
                movieTitlePublic.innerHTML = name;
                confirmPublic.setAttribute("data-id", id);
                confirmPublic.addEventListener('click', function(){
                    fetch("https://fathomless-woodland-43704.herokuapp.com/movies/public/"+id,{
                        method: 'PUT',
                        headers: {
                          'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({"is_public" : "true"})
                    })
                    .then (function(res){
                        console.log(res);
                        return res.text();
                    })
                    .then (function(data){
                        location.reload();
                    });
                })
            })
        })

        // For Edit Book modal
        editMovieBtn.forEach(function(btn){
            btn.addEventListener('click', function(){
                let id = this.parentElement.getAttribute("data-id");
                fetch("/movies/"+id+"/edit")
                .then (function(res){
                    return res.text();
                })
                .then (function(data){
                    editModalBody.innerHTML = data;

                    // for adding timeSlots
                    addTimeSlots.addEventListener('click', function(){
                        let timeSlotForm = document.createElement('div');
                        timeSlotForm.className = "form-group";
                        let input = document.createElement('input');
                        input.type = "time";
                        input.name = "timeSlots[]";
                        input.className = "form-control";
                        input.required = true;

                        timeSlotForm.appendChild(input);

                        additionalTimeSlots.appendChild(timeSlotForm);
                    })

                    // for removing timeslots
                    removeTimeSlots.addEventListener('click', function(){
                        if(additionalTimeSlots.childElementCount > 1)
                            additionalTimeSlots.lastElementChild.remove();
                    })
                });
            }); 
        })

        //show movie detail
        movieDetail.forEach(function(btn){
            btn.addEventListener('click', function(){
                let id = this.parentElement.getAttribute("data-id");
                let name = this.parentElement.getAttribute("data-name");
                movieTitle.innerHTML=name;
                fetch("/movies/"+id)
                .then (function(res){
                    return res.text();
                })
                .then (function(data){
                    detailModalBody.innerHTML = data;
                });
            }); 
        })

        $('#modalDetail').on('hide.bs.modal', function(){
            $('iframe').remove();
        })


    </script>
    
@endsection
