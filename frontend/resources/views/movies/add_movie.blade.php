@extends('layouts.app')

@section('title', 'Add Movie')

@section('content')
<h1>Add Movie</h1>

<form method="post" action="/movies/add" enctype="multipart/form-data" class="mb-5 addMovie">
	{{csrf_field()}}

	<div class="form-group">
		<label for="name">Movie Title:</label>
		<span class="redMark">*</span>
		<input type="text" name="name" class="form-control" required value="{{ old('name') }}">	
	</div>

	<div class="form-group">
		<label for="description">Description: </label>
		<span class="redMark">*</span>
		<textarea class="form-control" rows="5" name="description" required></textarea>
	</div>

	<div class="form-group">
		<label for="rating">Rating: </label>
		<span class="redMark">*</span>
		<select name="rating" class="form-control">
			@foreach($ratings as $rating)
				<option>{{$rating}}</option>
			@endforeach
		</select>
	</div>

	<div class="form-group">
		<label for="running_time">Runnning Time: </label>
		<span class="redMark">*</span>
		<input type="text" name="running_time" class="form-control" required value="{{ old('name') }}">	
	</div>

	<div class="form-group">
		<label for="date">Date of Showing:</label><span class="redMark">*</span>
		<input type="date" name="date" class="form-control" required value="{{ old('timeSlots') }}">
	</div>

	<div class="form-group">
		<label for="timeSlot">Time Slot:</label>
		<span class="redMark">*</span>
		<div class="float-right pb-3">
			<button class=" btn btn-info btn-sm" id="addTimeSlots" type="button">Add more</button>
			<button class=" btn btn-danger btn-sm" id="removeTimeSlots" type="button">Remove</button>
		</div>
		<input type="time" name="timeSlots[]" id="timeSlots" class="form-control" required value="{{ old('timeSlots') }}">
	</div>

	<div id="additionalTimeSlots">
	</div>

	<div class="form-group">
		<label for="cinema_id">Cinema:</label><span class="redMark">*</span>
		<select name="cinema_id" class="form-control">
			@foreach($cinemas as $cinema)
				<option value="{{$cinema->_id}}"> 
					{{$cinema->name}}
				</option>

			@endforeach
		</select>
	</div>

	<div class="form-group">
		<label for="poster">Poster:</label>
		<input type="file" name="poster" class="form-control-file">
	</div>
	<div class="form-group">
		<label for="trailer">Trailer Link:</label>
		<span class="redMark">*</span>
		<input type="text" name="trailer" class="form-control" value="{{ old('trailer') }}" required>	
	</div>

	<div class="form-group">
		<label for="price">Price:</label><span class="redMark">*</span>
		<input type="number" name="price" min="0" class="form-control" required 
		value="{{ old('price') }}">
	</div>

	<div class="form-check">
	  <input class="form-check-input" type="checkbox" value="true" id="is_public" name="is_public">
	  <label class="form-check-label" for="is_public">
	    Is Public? (You can't edit this after it goes live.)
	  </label>
	</div>

	<button class="btn btn-primary float-right" id="addBookBtn">Add</button>

</form>

<script type="text/javascript">
	addTimeSlots.addEventListener('click', function(){
		let timeSlotForm = document.createElement('div');
		timeSlotForm.className = "form-group";
		let input = document.createElement('input');
		input.type = "time";
		input.name = "timeSlots[]";
		input.className = "form-control";
		input.required = true;

		timeSlotForm.appendChild(input);

		additionalTimeSlots.appendChild(timeSlotForm);
	})

	removeTimeSlots.addEventListener('click', function(){
		additionalTimeSlots.lastChild.remove();
	})

</script>
@endsection