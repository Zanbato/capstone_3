<form method="post" action="/movies/{{$movie->_id}}/edit" enctype="multipart/form-data">
	{{csrf_field()}}

	<div class="form-group">
		<label for="name">Movie Title:</label><span class="redMark">*</span>
		<input type="text" name="name" class="form-control" required value="{{$movie->name}}">	
	</div>

	<div class="form-group">
		<label for="description">Description: </label>
		<textarea class="form-control" rows="5" name="description">{{$movie->description}}
		</textarea>
	</div>

	<div class="form-group">
		<label for="rating">Rating: </label>
		<select name="rating" class="form-control">
			@foreach($ratings as $rating)
				@if($rating == $movie->rating)
				<option selected>{{$rating}}</option>
				@else
				<option>{{$rating}}</option>
				@endif
			@endforeach
		</select>
	</div>

	<div class="form-group">
		<label for="running_time">Runnning Time: </label>
		<input type="text" name="running_time" class="form-control" required value="{{$movie->running_time}}">	
	</div>

	<div class="form-group">
		<label for="date">Date:</label><span class="redMark">*</span>
		<input type="date" name="date" class="form-control" required value="{{$movie->date}}">
	</div>


	<label for="timeSlots">Time Slots:</label><span class="redMark">*</span>
	<div class="float-right pb-3">
			<button class=" btn btn-info btn-sm" id="addTimeSlots" type="button">Add more</button>
			<button class=" btn btn-danger btn-sm" id="removeTimeSlots" type="button">Remove</button>
	</div>
	<div id="additionalTimeSlots">
	@foreach($movie->timeSlot as $slot)
		<div class="form-group">
			<input type="time" name="timeSlots[]" class="form-control" required value="{{ $slot->showingTime }}">
		</div>
	@endforeach

	</div>

	<div class="form-group">
		<label for="cinema_id">Cinema:</label><span class="redMark">*</span>
		<select name="cinema_id" class="form-control">
			@foreach($cinemas as $cinema)
				@if($cinema->_id == $movie->cinema_id)
					<option value="{{$cinema->_id}}" selected> 
						{{$cinema->name}}
					</option>
				@else
					<option value="{{$cinema->_id}}"> 
						{{$cinema->name}}
					</option>
				@endif
			@endforeach
		</select>
	</div>

	<div class="form-group">
		<label for="poster">Poster:</label>
		<input type="file" name="poster" class="form-control-file">
	</div>
	<div class="form-group">
		<label for="trailer">Trailer Link:</label><span class="redMark">*</span>
		<input type="text" name="trailer" class="form-control" required value="{{$movie->trailer}}">	
	</div>

	<div class="form-group">
		<label for="price">Price:</label><span class="redMark">*</span>
		<input type="number" name="price" min="0" class="form-control" required 
		value="{{ $movie->price }}">
	</div>


	<button class="btn btn-primary float-right" id="addBookBtn">Edit</button>

</form>