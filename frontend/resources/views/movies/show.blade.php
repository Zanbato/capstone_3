@extends('layouts.app')

@section('title', 'Now Showing')

@section('content')
<div class="row movieRow">

    @foreach($movies as $movie)
    <div class="col-lg-3 col-md-6 pt-3">
        <div class="card movieShow" data-id="{{$movie->_id}}" data-name="{{$movie->name}}">
            <div class="movieDetail" data-toggle="modal" data-target="#modalDetail">
                <img src="{{$movie->poster}}">
                <div class="alignTextBook">
                    <h5>{{$movie->name}}</h5>

                    <h6>{{date("F j, Y", strtotime($movie->date))}}</h6>
                </div>
            </div>
            <div class="card-footer timeSlotSched">
                <div class="row">{{$movie->cinema_name}}</div>
                <div class="row">
                    @foreach($movie->timeSlot as $timeslot)
                        @if(time() < strtotime($timeslot->showingTime))
                            <div class="col-6">
                                {{date("g:i a", strtotime($timeslot->showingTime))}}
                            </div>
                        @else
                            <div class="col-6 text-muted">
                                {{date("g:i a", strtotime($timeslot->showingTime))}}
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            @if(Session::has('user') && Session::get('user')->isAdmin)
                <a href="/transactions/{{$movie->_id}}">
                <button class="btn btn-info deleteMovieBtn w-100 mb-2">
                    Transactions
                </button>
                </a>
                <button class="btn btn-danger deleteMovieBtn w-100" data-toggle="modal" data-target="#modalDelete">Delete</button>
            @elseif(Session::has('user'))
                <button class="btn btn-warning buyMovieBtn w-100" data-toggle="modal" data-target="#modalBuy">Buy</button>
            @else
                <button class="btn btn-warning w-100" data-toggle="modal" data-target="#modalLogin">Buy</button>
            @endif
        </div>
    </div>
    @endforeach
        
</div>

{{-- Modal Book delete --}}
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
      <div class="modal-dialog .modal-dialog-centered " role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header ">
            <h5 class="modal-title">Remove Movie?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <!-- Modal Body -->
          <div class="modal-body" id="deleteModalBody">Are you sure you want to remove "<strong><span id="movieTitleDelete"></span></strong>" from Blockbuster?</div>

          <!-- Modal Footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="confirmDelete" data-dismiss="modal">Confirm Delete</button>
          </div>

        </div>
      </div>
    </div>

{{-- Modal Book Edit --}}
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header ">
            <h5 class="modal-title">Edit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <!-- Modal Body -->
          <div class="modal-body" id="editModalBody"></div>

        </div>
      </div>
    </div>

{{-- Modal Book login --}}
    <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header ">
            <h5 class="modal-title">Log in</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <!-- Modal Body -->
          <div class="modal-body" id="editModalBody">
            <form method="post" action="/users/login">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email" autofocus>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>

                <button type="submit" class="btn btn-primary float-right">Submit</button>

                <div>
                  <a href="/users/register">New to this site? Sign up here</a>
                </div>

            </form>
        </div>

        </div>
      </div>
    </div>

{{-- Modal Book details --}}
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header">
            <h3 class="modal-title" id="movieName"></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <!-- Modal Body -->
          <div class="modal-body" id="detailModalBody"></div>

        </div>
      </div>
    </div>

{{-- Modal Book buy --}}
    <div class="modal fade" id="modalBuy" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header">
            <h3 class="modal-title">Buy Tickets</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <!-- Modal Body -->
          <div class="modal-body" id="buyModalBody"></div>

        </div>
      </div>
    </div>

    {{-- Paypal --}}
    <script src="https://www.paypal.com/sdk/js?client-id=AYA_BXsCWsVRzVVtriXa70jp9nWkX_YMedoMbRGwxRwukWnu8C4OHcGI5OxfI6LNvIcxpvKJqj72JFnV&currency=PHP"></script>

    <script type="text/javascript">
        let deleteMovieBtn = document.querySelectorAll(".deleteMovieBtn");
        let editMovieBtn = document.querySelectorAll(".editMovieBtn");
        let buyMovieBtn = document.querySelectorAll(".buyMovieBtn");
        let movieDetail = document.querySelectorAll(".movieDetail");
        let modalDetail = document.querySelector("#modalDetail");


        deleteMovieBtn.forEach(function(btn){
            btn.addEventListener("click", function(){
                let id = this.parentElement.getAttribute("data-id");
                let name = this.parentElement.getAttribute("data-name");
                movieTitleDelete.innerHTML = name;
                confirmDelete.setAttribute("data-id", id);
                confirmDelete.addEventListener('click', function(){
                    fetch("https://fathomless-woodland-43704.herokuapp.com/movies/"+id,{
                        method: 'DELETE'
                    })
                    .then (function(res){
                        return res.text();
                    })
                    .then (function(data){
                        location.reload();
                    });
                })
            })
        })

        // For Edit Book modal
        editMovieBtn.forEach(function(btn){
            btn.addEventListener('click', function(){
                let id = this.parentElement.getAttribute("data-id");
                fetch("/movies/"+id+"/edit")
                .then (function(res){
                    return res.text();
                })
                .then (function(data){
                    editModalBody.innerHTML = data;
                });
            }); 
        })

        //For Detail movie button
        movieDetail.forEach(function(btn){
            btn.addEventListener('click', function(){
                let id = this.parentElement.getAttribute("data-id");
                let name = this.parentElement.getAttribute("data-name");
                movieName.innerHTML=name;
                fetch("/movies/"+id)
                .then (function(res){
                    return res.text();
                })
                .then (function(data){
                    detailModalBody.innerHTML = data;
                });
            }); 
        })
        $('#modalDetail').on('hide.bs.modal', function(){
            $('iframe').remove();
        })



        //for movie buy button
        buyMovieBtn.forEach(function(btn){
            btn.addEventListener('click', function(){
                let id = this.parentElement.getAttribute("data-id");
                fetch("/purchase/"+id)
                .then (function(res){
                    return res.text();
                })
                .then (function(data){
                    buyModalBody.innerHTML = data;
                    buyTickets();
                });
            }); 
        })

        //for movie buy modal
        function buyTickets(){

            let submitBtn = document.querySelector("#buyTickets")
            
            //select date
            schedule.addEventListener('change', function(){
                let id = this.parentElement.getAttribute('data-id')
                fetch('https://fathomless-woodland-43704.herokuapp.com/movies/'+id)
                .then (function(res){
                    return res.json();
                })
                .then (function(data){
                    let timeSlots = data.movie.timeSlot
                    timeSlots.forEach(function(slot){
                        if (slot._id == schedule.value) {
                        availableSeats.innerHTML = `Available seats: `+slot.seatsAvailable;
                            if (slot.seatsAvailable != 0){
                                quantity.disabled = false;
                                quantity.setAttribute('max', slot.seatsAvailable);
                                submitBtn.disabled = false;
                                totalDiv.hidden = false;
                                total.innerHTML= quantity.value*price.innerHTML
                            }
                        }
                    })
                });
            })
            //total changer
            quantity.addEventListener('input', function(){
                total.innerHTML= quantity.value*price.innerHTML
            })

            // Paypal
            paypal.Buttons({
            createOrder: function(data, actions) {
              return actions.order.create({
                purchase_units: [{
                  amount: {
                    value: total.innerHTML
                  }
                }]
              });
            },
            onApprove: function(data, actions) {
              return actions.order.capture().then(function(details) {
                // Call your server to save the transaction
                // let grandTotal = parseInt(total.innerHTML);
                // $.ajax({
                //         method:"post",
                //         url:"../controllers/process_checkout.php",
                //         data: {
                //             "total": grandTotal
                //         },
                //         success: function(data){
                //             fetch('../controllers/remove_all_items.php')
                //                 .then (function(res){
                //                     return res.text();
                //                 })
                //                 .then (function(data){
                //                     window.location.href = "transactions.php";
                //                 });
                //         }
                //     })
                buyTicketsForm.submit();
              });
            }
          }).render('#paypal-button-container');


        }
    </script>

    
@endsection