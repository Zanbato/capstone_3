<div class="row">
	<div class="col">
		<iframe width="496" height="279" src="{{$movie->trailer}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>
	<div class="col mt-2">
		<h5>Synopsis:</h5>
		<p>{!!nl2br(e($movie->description))!!}</p>
		<div class="small">Rating: {{$movie->rating}}</div>
		<div class="small">Running Time: {{$movie->running_time}}</div>
	</div>
</div>