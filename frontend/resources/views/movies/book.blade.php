<div class="row">
    <div class="col-6">
        <img src="/{{$movie->poster}}" width="100%">
    </div>
    <div class="col-6">
        <form method="post" action="/purchase/{{$id}}" id="buyTicketsForm">
            {{csrf_field()}}

            <div class="form-group">
                <label for="user">Name:</label>
                <input type="text" name="user" class="form-control" disabled 
                value="{{Session::get('user')->firstName}} {{Session::get('user')->lastName}}"> 
            </div>


            <div class="form-group">
                <label for="date">Date:</label>
                <input type="text" name="date" class="form-control" readonly value="{{$movie->date}}"> 
            </div>

            <div class="form-group" data-id="{{$movie->_id}}">
                <label for="schedule">Schedule:</label>
                <select class="form-control" id="schedule" name="timeSlot_id">
                    <option disabled selected>---</option>
                  @foreach($movie->timeSlot as $slot)
                    @if(time() < strtotime($slot->showingTime))
                    <option value="{{$slot->_id}}">{{date("g:i a", strtotime($slot->showingTime))}}</option>
                    @else
                    <option value="{{$slot->_id}}" disabled>{{date("g:i a", strtotime($slot->showingTime))}}</option>
                    @endif
                  @endforeach
                </select>
            </div>


            <div class="form-group">
                <label for="quantity">Quantity:</label>
                <div id="availableSeats"></div>
                <input type="number" name="quantity" id="quantity" class="form-control" disabled min="1" value="1" required>
            </div>

            <div id="priceDiv">
                Price: &#8369; <span id="price">{{$movie->price}}</span>
            </div>

            <div id="totalDiv" hidden>
                Total: &#8369; <span id="total"></span>
            </div>

            <button class="btn btn-primary form-control my-2" id="buyTickets" disabled>Checkout</button>
            <div id="paypal-button-container"></div>
        </form>
    </div>
</div>

