@extends('layouts.app')

@section('title')
Register
@endsection

@section('content')
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{Session::get('error')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <form method="post" action="/users/register" class="m-3">
        @csrf
        <div class="form-group">
            <label for="firstName">First name:</label>
            <input type="text" name="firstName" id="firstName" class="form-control registerInput" required placeholder="Enter first name..."  value="{{ old('firstName') }}">
            <span class="redMark"></span>
        </div>
        <div class="form-group">
            <label for="lastName">Last name:</label>
            <input type="text" name="lastName" id="lastName" class="form-control registerInput" required placeholder="Enter last name..." value="{{ old('lastName') }}">
            <span class="redMark"></span>
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" name="email" id="email" class="form-control registerInput" required placeholder="Email address" value="{{ old('email') }}">
            <span class="redMark"></span>
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" name="password" id="password" class="form-control registerInput" required placeholder="Password">
            <span class="redMark"></span>
        </div>
        <div class="form-group">
            <label for="confirmPassword">Verify password:</label>
            <input type="password" name="confirmPassword" id="confirmPassword" class="form-control registerInput" required placeholder="Password">
            <span class="redMark" id="errorMsg"></span>
        </div>
        <button class="btn btn-primary float-right" disabled id="submitBtn">Register</button>
    </form>

    <script>
        let inputs = document.querySelectorAll('.registerInput');

        inputs.forEach(function(userInput){
            userInput.addEventListener("input", function(){
                    if (this.value == "") {
                        this.nextElementSibling.innerHTML = "Field Required";
                    }
                    else{
                        this.nextElementSibling.innerHTML = "";
                    };

                    fieldCheck();
            });
        });

        function passwordCheck(x,y){
            if(x.value == "" || y.value == ""){
                errorMsg.innerHTML = "";
                return true;
            }
            else if(x.value != y.value){
                errorMsg.innerHTML = "Password did not match";
                return true;
            }
            else{
                errorMsg.innerHTML = "";
                return false;
            }
        };

        function fieldCheck(){
            let flag = false;

            flag = passwordCheck(password, confirmPassword);

            inputs.forEach(function(input){
                if (input.value == "") {
                    flag = true;
                }
            });

            if (!flag){
                submitBtn.disabled = false;
            }
            else{
                submitBtn.disabled = true;
            }
        };

        //create a function that will get the form data, convert it to json, and send it in the body of a fetch POST request to the register endpoint
    </script>
@endsection