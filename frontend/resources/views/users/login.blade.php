@extends('layouts.app')

@section('title')
Log-in
@endsection

@section('content')
<h1>Log In</h1>
@if(Session::has('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <strong>{{Session::get('error')}}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif
<div class="card mt-3" id="logIn">

    <form method="post" action="/users/login">
        {{csrf_field()}}
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email" autofocus>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>

        <button type="submit" class="btn btn-primary float-right">Submit</button>

        <div>
          <a href="/users/register">New to this site? Sign up here</a>
        </div>

    </form>

</div>

    <script>
        
    </script>
@endsection