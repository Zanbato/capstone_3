@extends('layouts.app')

@section('title', 'Cinemas')

@section('content')

    @if(Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{Session::get('message')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
    @elseif(Session::has('error'))
            <div class="alert alert-danger alert-dismissible fade show">
                {{Session::get('error')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
    @endif

    <div class="accordion" id="accordionCategory">

        <div class="card m-4">
            <div class="card-header bg-dark text-white">
                <div class="row">
                <div class="col">Cinema</div>
                @if(Session::has('user') && Session::get('user')->isAdmin)
                    <div class="col text-right">
                        <i class="fas fa-plus-circle fa-lg" 
                        data-toggle="collapse" 
                        data-target="#collapseOne" 
                        aria-expanded="true" 
                        aria-controls="collapseOne"></i>
                    </div>
                @endif
                </div>
            </div>
            @if(Session::has('user') && Session::get('user')->isAdmin)
                <div id="collapseOne" class="collapse" data-parent="#accordionCategory">
                  <div class="card-body">

                    <form method="post" action="/cinemas" class="pl-3">

                        {{csrf_field()}}
                        <div class="form-row text-justify">
                            <div class="col-7">
                                <input class="form-control cinemaInput" type="text" name="name" placeholder="New Cinema" id="nameInput">
                            </div>
                            <div class="col-4">
                                <input class="form-control cinemaInput" type="number" name="seatsAvailable" placeholder="Quantity" min="1" id="seatInput">
                            </div>
                            <div class="col">
                                <button class="btn btn-secondary" type="submit" id="addCatBtn" disabled>Add</button>
                            </div>
                        </div>

                    </form>
                  </div>
                </div>
            @endif
            <div class="card-body">

                <table class="table table-striped">
                    @foreach ($cinemas as $cinema)
                        <tr>
                            <td class="itemcategory">
                                <p>{{$cinema->name}}</p>
                                <p><small>Capacity: {{$cinema->seatsAvailable}}</small></p>
                                <form class="form-row text-justify">
                                    <div class="col">
                                        <input type="hidden" class="input-group form-control" name="name" value="{{$cinema->name}}">
                                    </div>
                                    <div class="col">
                                        <input type="hidden" class="input-group form-control" name="seatsAvailable" value="{{$cinema->seatsAvailable}}">
                                    </div>
                                </form>
                            </td>
                            <td class="buttons" data-id="{{$cinema->_id}}" data-name="{{$cinema->name}}" align="right">
                                @if(Session::has('user') && Session::get('user')->isAdmin)
                                    <button 
                                        class="btn btn-primary cinemaEdit">
                                        Edit
                                    </button>
                                    <button 
                                        class="btn btn-success saveCinemaEdit" 
                                        hidden>
                                        Save
                                    </button>
                                    <button 
                                        class="btn btn-danger deleteCinema" 
                                        name="id"
                                        data-toggle="modal"
                                        data-target="#modalDelete">
                                        Delete
                                    </button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>


{{-- Modal Book delete --}}
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
      <div class="modal-dialog .modal-dialog-centered " role="document">
        <div class="modal-content">

            <!-- Modal Header -->
          <div class="modal-header ">
            <h5 class="modal-title">Remove Cinema?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <!-- Modal Body -->
          <div class="modal-body" id="editModalBody">Are you sure you want to delete "<strong><span id="cinemaTitle"></span></strong>" from Blockbuster?</div>

          <!-- Modal Footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="confirmDelete" data-dismiss="modal">Confirm Delete</button>
          </div>

        </div>
      </div>
    </div>

    <script type="text/javascript">
        let cinemaInput = document.querySelectorAll('.cinemaInput');
        let cinemaEdit = document.querySelectorAll('.cinemaEdit');
        let saveCinemaEdit = document.querySelectorAll('.saveCinemaEdit');
        let deleteCinema = document.querySelectorAll('.deleteCinema');

    //For Category Add 
        cinemaInput.forEach(function(input){
            input.addEventListener('input', function(){
                fieldCheck();
            })     
        })

        function fieldCheck(){
            let inputFlag = false;

            cinemaInput.forEach(function(input){
                if (input.value == "") {
                    inputFlag = true;
                }
            });

            if(!inputFlag)
                addCatBtn.disabled = false;
            else
                addCatBtn.disabled = true;
        };


    // For Category Edit button
        cinemaEdit.forEach(function(btn){
            btn.addEventListener('click', function(){
                this.setAttribute('hidden', true);
                this.nextElementSibling.removeAttribute('hidden');
                this.parentElement.previousElementSibling.firstElementChild.remove();
                this.parentElement.previousElementSibling.firstElementChild.remove();
                this.parentElement.previousElementSibling.querySelector('input[name="name"]').setAttribute('type', 'text');        
                this.parentElement.previousElementSibling.querySelector('input[name="seatsAvailable"]').setAttribute('type', 'number');
                this.parentElement.previousElementSibling.querySelector('input[name="name"]').focus();     
                this.parentElement.previousElementSibling.querySelector('input[name="name"]').select();        
            });
        });

    // For Category Edit save
        saveCinemaEdit.forEach(function(btn){
            btn.addEventListener('click', function(){ 
                let editCinema = this.parentElement.previousElementSibling.querySelector('.form-row');   
                let data = new FormData(editCinema);
                let object = {};
                data.forEach(function(value, key){
                    object[key] = value;
                });
                let json = JSON.stringify(object);
                let id = this.parentElement.getAttribute('data-id');
                const headers = {
                    'Content-Type' : 'application/json',
                }

                fetch('https://fathomless-woodland-43704.herokuapp.com/cinemas/'+id, {
                    method:'put',
                    headers: headers,
                    body: json
                })
                .then(function(res){
                    return res.text();
                })
                .then(function(data){
                    location.reload();
                })
            });
        });

    // For Delete Category modal
        deleteCinema.forEach(function(btn){
            btn.addEventListener('click', function(){
                let id = this.parentElement.getAttribute("data-id");
                let name = this.parentElement.getAttribute("data-name");
                cinemaTitle.innerHTML = name;
                confirmDelete.setAttribute("data-id", id);
                confirmDelete.addEventListener('click', function(){
                    fetch("https://fathomless-woodland-43704.herokuapp.com/cinemas/"+id,{
                        method:'delete'
                    })
                    .then (function(res){
                        return res.text();
                    })
                    .then (function(data){
                        location.reload();
                    });
                })
            });
        });
        
    </script>
    
@endsection