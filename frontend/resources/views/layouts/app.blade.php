<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

  {{-- Bootstrap Style --}}
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


  	
	<title>@yield('title', 'MELN')</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="/"> Blockbuster <i class="fas fa-ticket-alt text-danger"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNavDropdown">

    <ul class="navbar-nav ml-auto">

      <li class="nav-item">
        <a class="nav-link" href="/now-showing"><i class="fas fa-film"></i> Now Showing</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/coming-soon"><i class="fas fa-calendar-day"></i>Coming Soon</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="https://www.imdb.com/news/movie" target="_blank"><i class="fas fa-newspaper"></i>Movie News</a>
      </li>
      @if(Session::has('user'))

        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                Hello, {{Session::get('user')->firstName}}
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                @if(Session::get('user')->isAdmin)
                  <a  class="dropdown-item" href="/movies/add">Add Movie</a>
                  <a  class="dropdown-item" href="/cinemas">Manage Cinemas</a>
                  <a class="dropdown-item" href="/admin/transactions">Transaction List</a>
                @else
                    <a class="dropdown-item" href="/user/transactions/">My Transactions</a>
                @endif
                
                <a class="dropdown-item" href="/logout">
                    Logout
                </a>
            </div>
        </li>

      @else

        <li class="nav-item ">
          <a class="nav-link text-danger" href="/users/register"><i class="fas fa-user"></i>Sign up</a>
        </li>

        <li class="nav-item dropdown">

          <a class="nav-link dropdown-toggle text-warning" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-sign-in-alt"></i>
            Login
          </a>

          <div class="dropdown-menu dropdown-menu-right">

            <form class="px-4 py-3" method="post" action="/users/login">
              {{csrf_field()}}
              <div class="form-group">
                <label for="exampleDropdownFormEmail1">Email address</label>
                <input type="email" class="form-control" id="FormEmail" name="email" placeholder="email@example.com">
              </div>

              <div class="form-group">
                <label for="exampleDropdownFormPassword1">Password</label>
                <input type="password" class="form-control" id="FormPassword" name="password" placeholder="Password">
              </div>
              
              <button type="submit" class="btn btn-primary btn-block">Sign in</button>

            </form>

            <div class="dropdown-divider"></div>

            <a class="dropdown-item" href="/users/register">New around here? Sign up</a>

          </div>
        </li>

      @endif
    </ul>

  </div>
</nav>
  
    
  {{-- Bootstrap --}}
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  @yield('index')
  <div class="container-fluid">
    @yield('content')
  </div>


 

<footer class="bg-dark pt-2 text-white">
  <div class="row mx-auto">   
      <div class="col-7 copyright small">
          <div class="disclaimer">&copy; DISCLAIMER: This site is for educational purposes only. I do not own nor claim to own any content within this site.</div>
      </div>
      <div class="col">
        <div class="float-right socialLinks">
          <a href="https://www.facebook.com/"><i class="fab fa-facebook-square"></i></a>
          <a href="https://twitter.com/"><i class="fab fa-twitter-square"></i></a>
          <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
          <a href="#"><i class="fas fa-share-alt-square"></i></a>
        </div>
      </div>
  </div>
</footer>


  <script type="text/javascript">
    let active = document.querySelector('a[href="'+window.location.pathname+'"]');
    if (active) {
      active.parentElement.classList.add("active")
      active.innerHTML += '<span class="sr-only">(current)</span>'
    }
  </script>
 



</body>
</html>