<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;

class MovieController extends Controller
{
    function show(Request $request){
    	$client = new Client();
    	$res = $client->get('https://fathomless-woodland-43704.herokuapp.com/movies');
    	$data = json_decode($res->getBody())->movies;
        $movies = [];
        foreach ($data as $movie) {
            if($movie->is_public == true)
                array_push($movies, $movie);
        }
    	return view('movies.show', compact('movies'));
    }

    function soon(Request $request){
        $client = new Client();
        $res = $client->get('https://fathomless-woodland-43704.herokuapp.com/movies');
        $data = json_decode($res->getBody())->movies;
        $movies = [];
        foreach ($data as $movie) {
            if($movie->is_public == false)
                array_push($movies, $movie);
        }
        return view('movies.comingsoon', compact('movies'));
    }
    
    function create(){
        if(Session::has('user') && Session::get('user')->isAdmin){
            $client = new Client();
            $res = $client->get('https://fathomless-woodland-43704.herokuapp.com/cinemas');
            $cinemas = json_decode($res->getBody())->cinema;
            $ratings = ['G', 'PG-13', 'R-13', 'R-16', 'R-18'];
            return view('movies.add_movie', compact('cinemas', 'ratings'));
        } else{
            return redirect('/');
        }
    }

    function store(Request $request){
        if($request->hasFile('poster')){
            $poster = $request->poster;
            $poster->move('assets/posters/', $poster->getClientOriginalName());
            $poster = 'assets/posters/'.$poster->getClientOriginalName();
        }
        else{
            $poster = 'assets/posters/No_picture_available.png';
        }

    	$client = new Client(); 
    	$request = $client->request('POST', 'https://fathomless-woodland-43704.herokuapp.com/movies', [
    	    'json' => [
    	        'cinema_id' => $request->cinema_id,
    	        'name' => $request->name,
                'description' => $request->description,
                'rating' => $request->rating,
                'running_time' => $request->running_time,
    	        'date' => $request->date,
    	        'timeSlots' => $request->timeSlots, //array
    	        'price' => $request->price,
    	        'poster' => $poster,
                'trailer' => $request->trailer,
                'is_public' => $request->is_public
            ]
        ]);
        return redirect('/now-showing');
    }

    function edit($id){
        $client = new Client();
        $res = $client->get("https://fathomless-woodland-43704.herokuapp.com/movies/$id");
        $movie = json_decode($res->getBody())->movie;
        $res = $client->get('https://fathomless-woodland-43704.herokuapp.com/cinemas');
        $cinemas = json_decode($res->getBody())->cinema;
        $ratings = ['G', 'PG-13', 'R-13', 'R-16', 'R-18'];
        return view('movies.edit_movie', compact('movie', 'cinemas', 'ratings'));
    }

    function update($id, Request $request){
        $jsonBody = [
            'cinema_id' => $request->cinema_id,
            'name' => $request->name,
            'description' => $request->description,
            'rating' => $request->rating,
            'running_time' => $request->running_time,
            'date' => $request->date,
            'timeSlots' => $request->timeSlots, //array
            'price' => $request->price,
            'trailer' => $request->trailer,
    	    'is_public' => $request->is_public
        ];
        if($request->hasFile('poster')){
            $poster = $request->poster;
            $poster->move('assets/posters/', $poster->getClientOriginalName());
            $poster = 'assets/posters/'.$poster->getClientOriginalName();
            $jsonBody['poster'] = $poster;
        }

        $client = new Client(); 
        $request = $client->request('PUT', 'https://fathomless-woodland-43704.herokuapp.com/movies/'.$id, [
            'json' => $jsonBody
        ]);
        return back();
    }

    function detail($id){
        $client = new Client();
        $res = $client->get('https://fathomless-woodland-43704.herokuapp.com/movies/'.$id);
        $movie = json_decode($res->getBody())->movie;
        return view('movies.detail', compact('movie'));
    }

     function book($id){
        $client = new Client();
        $res = $client->get('https://fathomless-woodland-43704.herokuapp.com/movies/'.$id);
        $movie = json_decode($res->getBody())->movie;
        return view('movies.book', compact('movie', 'id'));
    }

    function purchase($id, Request $request){
        $jsonBody = [
            'timeSlot_id' => $request->timeSlot_id,
            'email' => Session::get('user')->email,
            'firstName' => Session::get('user')->firstName,
            'lastName' => Session::get('user')->lastName,
            'quantity' => $request->quantity,
        ];

        $headers = [
            'Authorization' => "Bearer ".Session::get('user')->token,
            'Content-Type' => 'application/json'
        ];

        $client = new Client(); 
        try{   
            $request = $client->request('POST', 'https://fathomless-woodland-43704.herokuapp.com/transactions', [
                'headers' => $headers,
                'json' => $jsonBody
            ]);
        } catch(\GuzzleHttp \ Exception \ ClientException $e){
            if($e->getResponse()->getStatusCode() == 401){
                Session::flush();
                Session::flash('error', 'Session Timeout. Please Log in again.');
                return redirect('/users/login');
            }
        }
        return redirect('/user/transactions');
    }
}
