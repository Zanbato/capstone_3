<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;

class CinemaController extends Controller
{
    function show(){
    	$client = new Client();
    	$res = $client->get('https://fathomless-woodland-43704.herokuapp.com/cinemas');
    	$cinemas = json_decode($res->getBody())->cinema;
    	return view('cinemas.show', compact('cinemas'));
    }

    function store(Request $request){
    	$client = new Client(); 
    	$request = $client->request('POST', 'https://fathomless-woodland-43704.herokuapp.com/cinemas', [
    	    'json' => [
    	        'name' => $request->name,
                'seatsAvailable' => $request->seatsAvailable,
            ]
        ]);
        return redirect('/cinemas');
    }
    
}
