<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;

class TransactionController extends Controller
{
    function showAll(){
    	$headers = [
            'Authorization' => "Bearer ".Session::get('user')->token,
            'Content-Type' => 'application/json'
        ];

    	$client = new Client();
    	try{
	    	$res = $client->get('https://fathomless-woodland-43704.herokuapp.com/transactions/all',[
	    		'headers' => $headers
	    	]);
    	}
    	catch(\GuzzleHttp \ Exception \ ClientException $e){
    		if($e->getResponse()->getStatusCode() == 401){
                Session::flush();
                Session::flash('error', 'Session Timeout. Please Log in again.');
                return redirect('/users/login');
            }
    	}

    	$transactions = json_decode($res->getBody())->transactions;
    	$res = $client->get('https://fathomless-woodland-43704.herokuapp.com/movies');
    	$data = json_decode($res->getBody())->movies;
        $movies = [];
        foreach ($data as $movie) {
            if($movie->is_public == true)
                array_push($movies, $movie);
        }

    	return view('transactions.adminView', compact('transactions', 'movies'));
    }

    function show(){
    	$id = Session::get('user')->_id;
    	$headers = [
            'Authorization' => "Bearer ".Session::get('user')->token,
            'Content-Type' => 'application/json'
        ];

    	$client = new Client();
    	try{
	    	$res = $client->get('https://fathomless-woodland-43704.herokuapp.com/transactions/'.$id,[
	    		'headers' => $headers
	    	]);
    	}
    	catch(\GuzzleHttp \ Exception \ ClientException $e){
    		if($e->getResponse()->getStatusCode() == 401){
                Session::flush();
                Session::flash('error', 'Session Timeout. Please Log in again.');
                return redirect('/users/login');
            }
    	}

    	$transactions = json_decode($res->getBody())->transactions;

    	$res = $client->get('https://fathomless-woodland-43704.herokuapp.com/movies');
    	$data = json_decode($res->getBody())->movies;
        $movies = [];
        foreach ($data as $movie) {
            if($movie->is_public == true)
                array_push($movies, $movie);
        }

        return view('transactions.guestView', compact('transactions', 'movies'));
    }

    function showMovie($id){
        $headers = [
            'Authorization' => "Bearer ".Session::get('user')->token,
            'Content-Type' => 'application/json'
        ];

        $client = new Client();
        try{
            $res = $client->get('https://fathomless-woodland-43704.herokuapp.com/transactions/movie/'.$id,[
                'headers' => $headers
            ]);
        }
        catch(\GuzzleHttp \ Exception \ ClientException $e){
            if($e->getResponse()->getStatusCode() == 401){
                Session::flush();
                Session::flash('error', 'Session Timeout. Please Log in again.');
                return redirect('/users/login');
            }
        }

        $transactions = json_decode($res->getBody())->transactions;

        $res = $client->get('https://fathomless-woodland-43704.herokuapp.com/movies/'.$id);
        $movie = json_decode($res->getBody())->movie;

        return view('transactions.movieView', compact('transactions', 'movie'));
    }
}
