<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;

class UserController extends Controller
{
	function form(){
		if(Session::has('user'))
			return redirect('/');
		else
			return view('users.register');
	}

	function create(Request $request){
		$client = new Client();
		try{
			$respond = $client->request('POST', 'https://fathomless-woodland-43704.herokuapp.com/register', [
			    'json' => [
			        'firstName' => $request->firstName,
			        'lastName' => $request->lastName,
			        'email' => $request->email,
			        'password' => $request->password
			    ]
			]);
		}
		catch(\GuzzleHttp \ Exception \ ServerException $e){
			$respond = null;
		}
		if(!$respond){
			Session::flash('error', 'Email already exist');
			return redirect()->back()->withInput();
		}

		return redirect('/users/login');
	}

	function getLog(){
		if(Session::has('user'))
			return redirect('/');
		else
			return view('users.login');
	}

	function login(Request $request){
		$client = new Client(); 
		try{
			$respond = $client->request('POST', 'https://fathomless-woodland-43704.herokuapp.com/auth/login', [
			    'json' => [
			        'email' => $request->email,
			        'password' => $request->password,
			    ]
			]);
		}
		catch(\GuzzleHttp \ Exception \ ClientException $e){
			$respond = null;
		}
		if(!$respond){
			Session::flash('error', 'Invalid Credentials');
			return redirect('/users/login');
		}
		else{
			$user = json_decode($respond->getBody())->data;
			Session::put('user', $user);
			return back();
		}
	}

	function logout(){
		Session::flush();
		return redirect('/');
	}      
}
