<?php
Route::get('/', function () {
    return view('welcome');
});
Route::get('/logout', 'UserController@logout');

Route::get('users/login', 'UserController@getLog');

Route::get('users/register', 'UserController@form');

Route::post('users/register', 'UserController@create');
Route::post('users/login', 'UserController@login');

Route::get('/cinemas', 'CinemaController@show');
Route::post('/cinemas', 'CinemaController@store');
Route::get('/cinemas/delete/{id}', 'CinemaController@destroy');
Route::get('/now-showing', 'MovieController@show');
Route::get('/coming-soon', 'MovieController@soon');
Route::get('/movies/add', 'MovieController@create');
Route::get('/movies/{id}/edit', 'MovieController@edit');
Route::get('/movies/{id}', 'MovieController@detail');
Route::post('/movies/{id}/edit', 'MovieController@update');
Route::post('/movies/add', 'MovieController@store');
Route::get('/purchase/{id}', 'MovieController@book');
Route::post('/purchase/{id}', 'MovieController@purchase');

Route::get('/admin', function() {
	return view('users.admin');
});

//register a route that will return the update view from the cinemas directory when a GET request is sent to the /cinemas/update/{id} URI
//pass in the id wildcard as an argument to the compact function so that it can be used in the target view
Route::get('/cinemas/update/{id}', function($id) {
	return view('cinemas.update', compact('id'));
});


Route::get('/admin/transactions', 'TransactionController@showAll');
Route::get('/user/transactions/', 'TransactionController@show');
Route::get('/transactions/{id}', 'TransactionController@showMovie');